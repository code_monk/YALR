const color = require('bash-color');

module.exports = {
    "port": 9999,
    "socketPort": 9998,
    "appRoot": __dirname + '/app',
    "watch": true,
    "logger": (thing) => {
        //  good defaults for debugging.
        //  color codes the source for easy differentiation
        let thisColor = 'white';
        if ("color" in thing._meta) {
            thisColor = thing._meta.color;
        }
        let output = thing.payload;
        console.log(color[thisColor](JSON.stringify(output)));
    }
};
