# YALR

## Yet Another Live Reloader

This time done right. For rapid prototyping and frictionless HTML development.

I created this app because I was frustrated with how other Live Reload servers worked. In some cases, this may be just not understanding them, however, I feel an app that does Live Reloading should be very easy to unedersantd and "just work".

## Design Goals

1. Out of the box, it just works
2. Be minimal. Only support the "static server" paradigm.
3. Be less opinionated.
4. Configuration over Convention, with Sane Defaults to allow for #1, but with an extensible architecture allowing for modification of internals.
4. Honesty over Magic. We'll tell you exactly where to find (and how to modify) all the little bits that make live reloading work
5. Prefer simple browser reloading to fancy DOM injection. Other solutions try to get fancy and end up exposing bizzare edge case bugs. We just reload the browser any time there is a change. Period.

## Architecture

1. yalrconfig.js
2. Web Server
3. File Watcher process
4. Web Sockets Server
5. Web workers on the server

## Current Limitations

1. It only supports a static server right now. It will be hard to hack webServer.js to do otherwise

## Roadmap

1. extend webServer.js with middleware support
