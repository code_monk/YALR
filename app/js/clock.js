yalr.ready(() => {

    //  constants
    const ANIMATION_DURATION = 200;
    const ON_OPACITY = 1;
    const OFF_OPACITY = 0.2;

    //  variables
    let s = Snap('#clock');
    const dateOptions = {};
    var txt;
    const hours = s.selectAll('.hour > circle');
    const minutes = s.selectAll('.minute > circle');
    const seconds = s.selectAll('.second > circle');

    //  state
    const state = {
        timeShow: false
    };

    document.addEventListener('keypress',(ev) => {
        ev.preventDefault();
        const keyName = event.key;
        if (keyName === ' ') {
            state.timeShow = !(state.timeShow);
        }
    },false);

    let paint = () => {
        let now = new Date();
        let dateString = now.toLocaleTimeString('en-US',dateOptions);

        //  text time
        if (txt !== undefined) {
            txt.remove();    
        }
        txt = s.text("33%","75%",dateString);
        txt.attr({
            "font-size": "37",
            "fill": "white",
            "fill-opacity": Number(state.timeShow)
        });

        //  hours
        let hour = now.getHours();
        hours.forEach(node => {
            let hval = parseInt(node.attr('value'));
            if (hval <= hour) {
                node.animate({
                    "fill-opacity": ON_OPACITY
                },ANIMATION_DURATION);
                hour = hour - hval;
            } else {
                node.animate({
                    "fill-opacity": OFF_OPACITY
                },ANIMATION_DURATION);
            }
        });

        //  minutes
        let minute = now.getMinutes();
        minutes.forEach(node => {
            let mval = parseInt(node.attr('value'));
            if (mval <= minute) {
                node.animate({
                    "fill-opacity": ON_OPACITY
                },ANIMATION_DURATION);
                minute = minute - mval;
            } else {
                node.animate({
                    "fill-opacity": OFF_OPACITY
                },ANIMATION_DURATION);
            }
        });

        //  seconds
        let second = now.getSeconds();
        seconds.forEach(node => {
            let sval = parseInt(node.attr('value'));
            if (sval <= second) {
                node.animate({
                    "fill-opacity": ON_OPACITY
                },ANIMATION_DURATION);
                second = second - sval;
            } else {
                node.animate({
                    "fill-opacity": OFF_OPACITY
                },ANIMATION_DURATION);
            }
        });

    };

    let intervalID = window.setInterval(window.requestAnimationFrame.bind(null,paint),1000);

});
