"use strict";

//  our one and only global object
const yalr = {
    initialized: false,
    version: "1.0.1",
    config: null,
    events: null
};
yalr.ready = (func) => {
    if (typeof func !== "function") {
        throw Error('you didn\'t pass a function to yalr.ready()')
    }
    if (yalr.initialized) {
        func();
    } else {
        document.addEventListener('yalrReady',func);
    }
};

{
    const yalrReady = new Event('yalrReady');

    let handleFileChange = (msg) => {
        let obj = msg.payload;
        switch(obj.eventType) {
            case 'change':
            case 'rename':
            document.location.reload(true);
            break;
            default:
            console.log('obj.eventType was ' + obj.eventType,obj);
            break;
        }
    };

    let injectSocketClient = () => {
        /**
         * @TODO: Make this the first <scipt> element, rather than the last
         **/
        return new Promise((resolve,reject) => {
            let scriptPath = 'http://localhost:'+yalr.config.socketPort+'/socket.io/socket.io.js';
            let head = document.getElementsByTagName("head")[0];
            var oScript = document.createElement("script");
            oScript.type = "text\/javascript";
            oScript.onerror = reject;
            oScript.onload = resolve;
            head.appendChild(oScript);
            oScript.src = scriptPath;
        });
    };

    let init = () => {
        fetch('/yalrconfig.json')
        .then(response => {
            return response.json();
        })
        .then(json => {
            yalr.config = json;
            return Promise.resolve(true);
        })
        .then(injectSocketClient)
        .then(baton => {
            return new Promise((resolve,reject) => {
                yalr.events = io('http://localhost:'+yalr.config.socketPort+'/yalr');
                yalr.events.on('connect',() => {
                    resolve(true);
                });
            });
        }).then(baton => {
            yalr.events.on('filechange', handleFileChange);
            yalr.initialized = true;
            document.dispatchEvent(yalrReady);
            yalr.events.emit('debug','yalr is ready');
            return Promise.resolve(true);
        });
    };

    //  run code on DOM ready
    window.addEventListener('load',(ev) => {
        //console.log('window was just LOADED');
    });
    if (document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded',init);
    } else {
        init();
    }
}
