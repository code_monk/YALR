"use strict";

{
    let debug = (stuff) => {
        console.log(stuff);
    }
    let init = () => {
        yalr.events.on('debug', debug);
    };
    if (typeof yalr === "undefined" || !(yalr.initialized)) {
        document.addEventListener('yalrReady',init);
    } else {
        init();
    }
}
