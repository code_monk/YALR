/**
 * YALR File Watcher. Watches files and then notifies Socket Server
 *
 **/

const fs = require('fs');
const config = require(process.env.PWD+'/yalrconfig.js');
const msg = require(process.env.PWD+'/server/msg.js');

const log = (obj) => {
    let rawMessage = msg('fileWatcher',obj,'purple');
    self.postMessage( rawMessage );
};

const listener = (eventType, filename) => {
    log({eventType, filename});
};

fs.watch( config.appRoot, {"recursive": true, "persistent": true}, listener );
