const Worker = require("tiny-worker");
const config = require(process.env.PWD+'/yalrconfig.js');

/**
 * Spawn the three processes we need, and act as a message bus between them
 **/

module.exports = () => {

    //  launch web server
    const webServer = new Worker(__dirname+'/webServer.js');
    webServer.onmessage = (ev) => {
        config.logger(ev.data);
    };

    //  launch file watcher
    const fileWatcher = new Worker(__dirname+'/fileWatcher.js');
    fileWatcher.onmessage = (ev) => {
        socketServer.postMessage(ev.data);
        config.logger(ev.data);
    };

    //  socket server
    const socketServer = new Worker(__dirname+'/socketServer.js');
    socketServer.onmessage = (ev) => {
        config.logger(ev.data);
    };

};
