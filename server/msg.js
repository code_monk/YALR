/**
 * A common message format for YALR components
 **/

module.exports = (source,payload,color) => {
    if (!(typeof color === "string")) {
        color = 'white';
    }
    if (payload === null || !(typeof payload === 'object')) {
        payload = {payload};
    }
    return {
        "_meta": {
            "ztamp": Date.now(),
            "source": source,
            "color": color
        },
        "payload": payload
    };
};
