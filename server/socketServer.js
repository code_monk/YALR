/**
 * YALR Socket Server. The socket server, responsible for sending file system changes to the front end
 *
 **/

const config = require(process.env.PWD+'/yalrconfig.js');

const opts = { 
    serveClient: true,
    origins: 'localhost:'+config.port
};
var Server = require('socket.io');
var io = new Server(9998,opts);
var yalrevents = io.of('/yalr');

const log = (msg) => {
    let rawMessage = {
        "_meta": {
            "source": "socketServer",
            "ztamp": Date.now(),
            "color": "green"
        },
        "payload": null
    };
    if (typeof msg === 'object') {
        rawMessage.payload = msg;    
    } else {
        rawMessage.payload = {msg};
    }
    self.postMessage( rawMessage );
};

yalrevents.on('connection', function (socket) {
  yalrevents.emit('debug', 'socket connected');
  socket.on('debug', function(msg) {
    log(msg);
    yalrevents.emit('debug',msg);
  });
  socket.on('disconnect', function() {
    log('socket disconnected');
  });
});

self.onmessage = (ev) => {
    log(ev);
    yalrevents.emit('filechange', ev.data);
};
