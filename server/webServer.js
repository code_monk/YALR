/**
 * YALR Web Server. The static server component of the YALR system.
 *
 **/

const http = require('http');
const mimeTypes = require('mime-types');
const fs =require('fs');
const config = require( process.env.PWD+'/yalrconfig.js');

const log = (req,res,err) => {
    let rawMessage = {
        "_meta": {
            "source": "webServer",
            "ztamp": Date.now(),
            "color": "blue"
        },
        "payload": {
            "url": req.url,
            "statusCode": res.statusCode,
            "error": err,
            "bytes": res.bytes,
        }
    };
    self.postMessage( rawMessage );
};

const doError = (req,res,err) => {
    let output = 'yuk! something terrible happened';
    res.writeHead(500,{
        "Content-Type": mimeTypes.lookup('txt')
    });
    res.bytes = output.length;
    res.write(output);
    res.end('\n');
    log(req,res,err);
}

const doGood = (req,res,data) => {
    let mimeType = mimeTypes.lookup(req.url);
    let extension = mimeTypes.extension(mimeType);
    switch (extension) {
        case 'svg':
        res.setHeader('Content-Type', mimeType);
        res.setHeader('Vary', 'Accept-Encoding');
        break;
        default:
        res.setHeader('Content-Type', mimeType);
        break;
    }
    res.bytes = data.length;
    res.write(data);
    res.end('\n');
    log(req,res,null);
};

const handler = (req,res) => {
    switch (req.url) {
        case '/js/yalrconfig.js':
        case '/yalrconfig.js':
        res.setHeader('Access-Control-Allow-Origin','*');
        doGood(req,res,'yalr.config = '+JSON.stringify(config)+';');
        break;
        case '/json/yalrconfig.json':
        case '/yalrconfig.json':
        res.setHeader('Access-Control-Allow-Origin','*');
        doGood(req,res,JSON.stringify(config));
        break;
        default:
        fs.readFile(config.appRoot+req.url, 'utf8', (err,data) => {
            if (err) {
                doError(req,res,err.message);
            } else {
                doGood(req,res,data);
            }
        });
        break;
    }
};

http.createServer(handler).listen(config.port);
